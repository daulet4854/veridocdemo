//
//  ResultViewController.swift
//  VeridocDemoSwift
//
//  Created by Daulet Tungatarov on 8/16/19.
//  Copyright © 2019 Daulet Tungatarov. All rights reserved.
//

import UIKit

class ResultViewController: UITableViewController {
    
    var results: [(key: String, value: Any)] = []
    
    private let cellIdentifier = String(describing: TitleDescriptionCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(TitleDescriptionCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TitleDescriptionCell else {
            return UITableViewCell()
        }
        cell.set(title: results[indexPath.row].key)
        if let value = results[indexPath.row].value as? String {
            cell.set(description: value)
        } else {
            cell.set(description: "")
        }
        return cell
    }
}
