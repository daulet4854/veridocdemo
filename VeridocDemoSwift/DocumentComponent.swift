//
//  DocumentComponent.swift
//  VeridocDemoSwift
//
//  Created by Daulet Tungatarov on 8/16/19.
//  Copyright © 2019 Daulet Tungatarov. All rights reserved.
//

import UIKit

struct DocumentComponent {
    var identifier: DocumentComponentIdentifier
    var image: UIImage?
    var description: String
    var result: [(key: String, value: Any)]?
}

enum DocumentComponentIdentifier {
    case front
    case back
}
