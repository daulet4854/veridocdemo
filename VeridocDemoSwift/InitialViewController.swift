//
//  InitialViewController.swift
//  VeridocDemoSwift
//
//  Created by Daulet Tungatarov on 8/16/19.
//  Copyright © 2019 Daulet Tungatarov. All rights reserved.
//

import UIKit
import VeridocSDK

class InitialViewController: UIViewController {
    
    private let tableView = UITableView(frame: .zero, style: .grouped)
    private let continueButton = UIButton(type: .system)
    
    private let headerIdentifier = String(describing: TitleHeader.self)
    private let cellIdentifier = String(describing: DocumentAttachmentCell.self)
    
    private var components: [DocumentComponent] = [
        DocumentComponent(identifier: .front, image: nil, description: "Сканировать лицевую сторону", result: nil),
        DocumentComponent(identifier: .front, image: nil, description: "Сканировать оборотную сторону", result: nil)
    ]
    private var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSubviews()
        setViewConstraints()
        stylizeViews()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(TitleHeader.self, forHeaderFooterViewReuseIdentifier: headerIdentifier)
        tableView.register(DocumentAttachmentCell.self, forCellReuseIdentifier: cellIdentifier)
        
        continueButton.addTarget(self, action: #selector(proceedToPerformNextStep), for: .touchUpInside)
    }
    
    private func addSubviews() {
        view.addSubview(tableView)
        view.addSubview(continueButton)
    }
    
    private func stylizeViews() {
        view.backgroundColor = .white
        
        tableView.separatorStyle = .none
        tableView.estimatedSectionHeaderHeight = 100
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        
        continueButton.backgroundColor = .lightGray
        continueButton.setTitleColor(.white, for: .normal)
        continueButton.titleLabel?.font = .boldSystemFont(ofSize: 17)
        continueButton.setTitle("Продолжить", for: .normal)
        continueButton.layer.cornerRadius = 8
        continueButton.isEnabled = false
    }
    
    private func setViewConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.bottomAnchor.constraint(equalTo: continueButton.topAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ]
        
        continueButton.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            continueButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 14),
            continueButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8),
            continueButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -14),
            continueButton.heightAnchor.constraint(equalToConstant: 50)
        ]

        NSLayoutConstraint.activate(layoutConstraints)
    }
    
    private func setContinueButton(enable: Bool) {
        continueButton.isEnabled = enable
        continueButton.backgroundColor = enable ? UIColor(red: 34/255, green: 139/255, blue: 34/255, alpha: 1) : .lightGray
    }
    
    @objc private func proceedToPerformNextStep() {
        var results = [(key: String, value: Any)]()
        components.forEach { results += $0.result ?? [] }
        
        let resultViewController = ResultViewController()
        resultViewController.results = results
        navigationController?.pushViewController(resultViewController, animated: true)
    }
    
    private func showScanner() {
        let viewController = ScannerViewController()
        viewController.delegate = self
        present(viewController, animated: true)
    }
    
    private func checkComponentsFilling() {
        for component in components {
            if component.result == nil {
                setContinueButton(enable: false)
                return
            }
        }
        setContinueButton(enable: true)
    }
}

extension InitialViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return components.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DocumentAttachmentCell else {
            return UITableViewCell()
        }
        cell.set(description: components[indexPath.row].description)
        return cell
    }
}

extension InitialViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as? TitleHeader else {
            return UIView()
        }
        header.set(title: "Чтобы продолжить, отсканируйте документ")
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        showScanner()
    }
}

extension InitialViewController: ScannerViewControllerDelegate {
    
    func onSuccessCallback(_ result: [AnyHashable : Any]!) {
        print("result:", result ?? [:])
        guard let result = result as? [String: Any], let indexPath = selectedIndexPath else { return }
        components[indexPath.row].result = Array(result)
        
        dismiss(animated: true) { [weak self] in
            self?.components[indexPath.row].description += " ✅"
            self?.checkComponentsFilling()
            self?.tableView.reloadData()
        }
    }
    
    func onErrorCallback(_ statusCode: Int32, withMessage message: String!) {
        print("error:", message ?? "")
    }
    
    func scannerSuspendedByUserAction() {
        print("dismissed..")
        dismiss(animated: true)
    }
}
