//
//  DocumentAttachmentCell.swift
//  VeridocDemoSwift
//
//  Created by Daulet Tungatarov on 8/16/19.
//  Copyright © 2019 Daulet Tungatarov. All rights reserved.
//

import UIKit

class DocumentAttachmentCell: UITableViewCell {
    
    private let whiteView = UIView()
    private let descriptionLabel = UILabel()
    private let arrowImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubviews()
        setViewConstraints()
        stylizeViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        descriptionLabel.text = nil
        arrowImageView.image = UIImage(named: "smallRightArrow")
    }
    
    private func addSubviews() {
        contentView.addSubview(whiteView)
        
        whiteView.addSubview(descriptionLabel)
        whiteView.addSubview(arrowImageView)
    }
    
    private func stylizeViews() {
        selectionStyle = .none
        backgroundColor = .clear
        
        whiteView.backgroundColor = .white
        whiteView.layer.cornerRadius = 8
        
        descriptionLabel.font = .systemFont(ofSize: 17)
        descriptionLabel.numberOfLines = 0
        
        arrowImageView.image = UIImage(named: "smallRightArrow")
    }
    
    private func setViewConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()
        
        whiteView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            whiteView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            whiteView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 14),
            whiteView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            whiteView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -14)
        ]
        
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            descriptionLabel.topAnchor.constraint(equalTo: whiteView.topAnchor, constant: 16),
            descriptionLabel.leftAnchor.constraint(equalTo: whiteView.leftAnchor, constant: 16),
            descriptionLabel.bottomAnchor.constraint(equalTo: whiteView.bottomAnchor, constant: -16),
            descriptionLabel.rightAnchor.constraint(equalTo: arrowImageView.leftAnchor, constant: -8),
            descriptionLabel.heightAnchor.constraint(equalToConstant: 50)
        ]
        
        arrowImageView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            arrowImageView.centerYAnchor.constraint(equalTo: whiteView.centerYAnchor),
            arrowImageView.rightAnchor.constraint(equalTo: whiteView.rightAnchor, constant: -14),
            arrowImageView.widthAnchor.constraint(equalToConstant: 8),
            arrowImageView.heightAnchor.constraint(equalToConstant: 14)
        ]
        
        NSLayoutConstraint.activate(layoutConstraints)
    }
    
    func set(description: String) {
        descriptionLabel.text = description
    }
}

