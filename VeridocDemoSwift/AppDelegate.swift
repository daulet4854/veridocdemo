//
//  AppDelegate.swift
//  VeridocDemoSwift
//
//  Created by Daulet Tungatarov on 8/15/19.
//  Copyright © 2019 Daulet Tungatarov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = UINavigationController(rootViewController: InitialViewController())
        
        return true
    }
}

